require "config"

module ProgramConfig
  def self.read_config(string)
    cfg = {} of String => String | Bool | Time

    begin
    Config.parse(string).as_h.each do |k, v|
      if %w[title output lang2 langfb source config cover style lang1 votive horas rubrics datefrom dateto numofdays].index(k)
        cfg[k] = v.as_s
      else
        cfg[k] = v.as_bool
      end
    end
    rescue
      abort "Wrong data in config file:\n#{string}"
    end
    cfg
  end
end
