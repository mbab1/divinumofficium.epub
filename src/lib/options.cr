require "option_parser"

module Options
  DO_DATEFORMAT = "%m-%d-%Y"
  DO_WPATH = "/cgi-bin/horas/"
  DO_LPATH = "/web#{DO_WPATH}"

  macro default(name)
    {% unless name == "datefrom" || name == "dateto" || name == "numofdays" %}
    "[#{opts({{name}})}]"
    {% else %}
    ""
    {% end %}
  end

  macro switch(long, description)
    parser.on("--#{{{long}}}",
                ({{description}}  || "divinum officium option")) { @@opts[{{long}}] = !opts({{long}}) }
  end

  macro switch_with_short(short, long, description)
    parser.on("-#{{{short}}}", "--#{{{long}}}",
                {{description}} + " " + default {{long}}) { @@opts[{{long}}] = !opts({{long}}) }
  end

  macro option(short, long, name, description)
    {% if short.empty? %}
    parser.on("--#{{{long}}}=#{{{name}}}",
                {{description}} + " " + default {{long}}) { |val| @@opts[{{long}}] = val }
    {% else %}
    parser.on("-#{{{short}}} #{{{name}}}", "--#{{{long}}}=#{{{name}}}",
                {{description}} + " " + default {{long}}) { |val| @@opts[{{long}}] = val }
    {% end %}
  end

  macro option_wd(short, long, name, description)
    parser.on("-#{{{short}}} #{{{name}}}", "--#{{{long}}}=#{{{name}}}",
                {{description}}) { |val| @@opts[{{long}}] = val }
  end

  macro values(name)
    "( #{{{name}}.join(" | ")} )"
  end

  macro wrong_options(name)
    abort "#{{{name.stringify}}[10..-2].capitalize} must be one of: #{{{name}}.join(" | ")}"
  end

  private def self.check_date(name, today)
    value = @@opts[name].as(String).strip
    value += today.to_s("/%Y") if value.matches? %r{^\d\d?[-/]\d\d?$}
    value = value.gsub(%r{/}, "-")
    return Time.parse_local(value, DO_DATEFORMAT)
  rescue
    abort "Wrong #{name} date: #{@@opts[name]}"
  end

  private def self.check_source(source)
    uri = URI.parse source
    if uri.scheme
      source += DO_WPATH if uri.path
    else
      if (File::Info.readable? source + DO_LPATH)
         source += DO_LPATH
      else
        abort "Can't locate #{DO_LPATH} in #{source}"
      end
    end
    source
  end

  private def self.unequivocal(array, value)
    r = array.select { |x| x =~ /#{value}/ }
    if r.size == 1
      r.first
    else
      r = array.select { |x| x == value }
      if r.size == 1
        r.first
      else
        if r.size > 1
          puts "ambigous *#{value}* match #{r.join(" and ")}"
        end
        false
      end
    end
  end

  private def self.check_horas
    a = [] of String
    @@opts["horas"].as(String).split(/(?=\p{Lu}\p{Ll}*)/).each do |i|
      unless o = unequivocal(@@do_opts["HORAS"], i)
        wrong_options @@do_opts["HORAS"]
      end
      a.push o.as(String)
    end
    a.join("")
  end

  private def self.check_options
    if opts("config")
      if !File::Info.readable?(opts("config").as(String))
        abort "Can't read config file #{opts("config")}"
      else
        @@opts.merge! ProgramConfig.read_config File.read @@opts["config"].as(String)
      end
    end
    @@opts["source"] = check_source(opts("source").as(String))
    load_do_options
    if opts("style") && !File::Info.readable?(opts("style").as(String))
      abort "Can't read style file #{opts("style")}"
    end
    if opts("cover") && !File::Info.readable?(opts("cover").as(String))
      abort "Can't read cover file #{opts("cover")}"
    end
    if opts("fontdir") && !Dir.exists?(opts("fontdir").as(String))
      abort "Can't read font directory #{opts("fontdir")}"
    end
    if !opts("overwrite") && File.exists? opts("output").as(String)
      abort "Output file #{opts("output")} already exist. Change --output  or use --overwrite "
    end
    unless @@opts["votive"] = unequivocal(@@do_opts["VOTIVES"], opts("votive").as(String))
      wrong_options @@do_opts["VOTIVES"]
    end
    unless @@opts["rubrics"] = unequivocal(@@do_opts["RUBRICS"], opts("rubrics").as(String))
      wrong_options @@do_opts["RUBRICS"]
    end
    unless @@opts["lang2"] = unequivocal(@@do_opts["LANGUAGES"], opts("lang2").as(String))
      wrong_options @@do_opts["LANGUAGES"]
    end
    unless @@opts["langfb"] = unequivocal(@@do_opts["LANGUAGES"], opts("langfb").as(String))
      wrong_options @@do_opts["LANGUAGES"]
    end
    @@opts["nocoverpage"] = true if opts("nocover")
    @@opts["title"] = "Officium #{@@opts["votive"].as(String).sub(%r{/.*}, "")}" unless @@opts["votive"] == "Hodie"
    @@opts["horas"] = check_horas
    today = Time.local
    @@opts["datefrom"] = @@opts.has_key?("datefrom") ? check_date("datefrom", today) : (today + 1.day)
    @@opts["dateto"] = @@opts.has_key?("dateto") ? check_date("dateto", today) : @@opts["datefrom"].as(Time).at_end_of_month
    if opts("numofdays")
      @@opts["dateto"] = @@opts["datefrom"].as(Time) + (@@opts["numofdays"].as(String).to_i - 1).day
    end
  end

  @@opts  = {} of String => String | Bool | Time

  def self.opts(opt)
    @@opts[opt]?
  end

  @@do_opts = Hash(String, Array(String)).new

  def self.do_opts(opt)
    @@do_opts[opt]? || [] of String
  end

  private def self.load_do_options
    src = @@opts["source"].as(String)

    if src.matches?(%r{^https?://})
      data = Assets.get_file("horas.dialog").read
    elsif
      begin
        data = File.read(src + "../../www/horas/horas.dialog")
      rescue
        abort "Can't locate horas.dialog in #{@@opts["source"]}"
      end
    end

    %w(versions languages votives horas).each do |o|
      @@do_opts[o.upcase] = data.sub(%r{.*\[#{o}\].}m, "").sub(/\n\n.*/m, "").split(/,/).as(Array)
    end
    @@do_opts["RUBRICS"] = @@do_opts["VERSIONS"].map { |x| x.sub(%r{/.*}, "") }
    @@do_opts.delete("VERSIONS")
    @@do_opts["HORAS"].push "Omnes"
  end

  def self.init
    @@opts = ProgramConfig.read_config Assets.get_file "config"
    @@opts["lang1"] = "Latin"
    load_do_options

    OptionParser.parse do |parser|
      parser.banner = "Usage: #{Path[PROGRAM_NAME].basename} <arguments>
        default values in []
        possible vaulues in ()"
      parser.separator "\nDivinum officium options:"
      option_wd "r", "rubrics", "RUBRICS", values(@@do_opts["RUBRICS"])
      option "f", "datefrom", "DATE", "start date MM-DD-YYYY ['tomorrow']"
      option "t", "dateto", "DATE", "end date MM-DD-YYYY ['end of month of start date']"
      option "n", "numofdays", "NUMBER", "give number of days instead of above - no defaults"
      option "H", "horas", "HORAS", "string consist horas ex. 'VesperaeCompletorium'"
      option "l", "lang2", "LANGUAGE", "language for right side #{values @@do_opts["LANGUAGES"]}"
      option "b", "langfb", "LANGUAGE", "fallback language for missing translation #{values @@do_opts["LANGUAGES"]}"
      # option "g", "lang1", "LANGUAGE", "language for left side as above"
      option "e", "votive", "VOTIVE", values @@do_opts["VOTIVES"]
      switch "priest", "priest mode"
      switch "oldhymns", "use pre Urban VII hymns"
      switch "nonumbers", "do not number verses in psalms, canticles, biblical reading"
      switch "noexpand", "do not expand common prayes"
      switch "nofancychars", "use + for crosses, VR for ℣℟"
      parser.separator "\nProgram options:"
      option "o", "output", "FILE", "epub file name"
      switch "overwrite", "overwrite output file"
      option "i", "title", "TITLE", "book title"
      switch "nocover", "do not include cover"
      switch "nocoverpage", "do not insert cover page"
      option_wd "k", "cover", "COVERFILE", "cover image file"
      switch "notitlepage", "do not insert title page"
      switch "index", "insert index page"
      switch "antepost", "insert page with Apéri & Sacrosánctæ"
      switch "nocomments", "omit comments"
      switch "noomitted", "omit omitted"
      option_wd "s", "style", "CSSFILE", "style sheet file"
      parser.on("-S", "--dumpcss", "show internal css style") {
        puts Assets.get_file("style.css"); exit }
      option_wd "d", "fontdir", "DIR", "include fonts from directory"
      option_wd "p", "source", "SOURCE", "path/url to divinum officium in place of #{@@opts["source"]}"
      option_wd "c", "config", "CFGFILE", "read options from file"
      parser.on("-C", "--dumpconfig", "show default configuration") {
        puts Assets.get_file("config"); exit }
      switch_with_short "q", "quiet", "do not report progress"
      parser.on("--version", "show program version and exit") {
        puts "#{Path[PROGRAM_NAME].basename} version #{VERSION}"; exit }
      parser.on("-h", "--help", "show this message and exit") {
        puts parser; exit }
      parser.unknown_args { |x|
        abort "Unknown option: #{x.join(" ")}\nRun '#{Path[PROGRAM_NAME].basename} -h' for help" unless x.empty?
      }
    end
    check_options
  end
end
