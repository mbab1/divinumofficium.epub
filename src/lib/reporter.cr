module Reporter
  @@last_length = 0

  def self.report(s)
    return if Options.opts("quiet")
    print "\r#{"".ljust(@@last_length)}\r#{s}" 
    @@last_length = s.size
  end
end
