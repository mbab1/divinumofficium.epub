require "baked_file_system"

module Assets
  class FileStorage
    extend BakedFileSystem

    bake_folder "../../assets"
  end

  def self.get_file(name)
    f = uninitialized BakedFileSystem::BakedFile
    begin
      f = FileStorage.get(name)
    rescue BakedFileSystem::NoSuchFileError
      abort "Internal file #{name} is missing!"
    end
  end
end

