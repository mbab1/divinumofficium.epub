require "lexbor"

struct Lexbor::Node
  def add_class(klass)
    self["class"] = if has_key? "class"
                      (self["class"]?.as(String).split + [klass]).join(" ")
                    else
                      klass
                    end
    self
  end
end

class Lexbor::Parser
  private def font_class(v)
    if v === "1" || v === "82%"
      "text-sm"
    elsif v === "-1"
      "text-sm"
    elsif v === "+1" || v === "1.25em"
      "text-lg"
    elsif v === "+2"
      "text-xl"
    else
      raise "Unknown font settings #{v}"
    end
  end

  def font_spans
    nodes(:span).each do |f|
      a = [] of String
      f.attributes.each do |k, v|
        if k === "style"
          v.split(/\s*;\s*/).each do |x|
            next if x.empty?
            y, z = x.split(/:/) 
            if y === "font-size"
              a.push font_class(z)
            elsif y == "color"
              a.push z
            else 
              raise "Unknown span style #{y}"
            end
          end
        else
          a.push (if v.matches? /[a-z]+/i
                    v.downcase
                  else
                    font_class(v)
                  end)
        end
        f.attribute_remove k
      end
      a.each { |c| f.add_class c }
    end
    self
  end

  def remove_tags( tags )
    tags.each { |t| nodes(t).to_a.each(&.remove!) }
    self
  end

  macro cell(content, lang, pos)
    newcell = create_node(:div)
    newcell["class"] = "table-cell lang{{pos}}"
    newcell["lang"] = lang[{{lang}}]
    newcell.inner_html = {{content}}
    newrow.append_child(newcell)
  end

  def divs(lang1, lang2)
    lang = Hash.zip(Options.do_opts("LANGUAGES"), Options.do_opts("LANGUAGES").map { |x| x[0..1].downcase })
    lang["Polski"] = lang["Polski-New"] = "pl"
    lang["Magyar"] = "hu"
    lang["Čeština/Bohemice"] = "cs"
    css("div[align=right]").each(&.remove!)
    nodes("table").each do |table|
      newdiv = create_node(:div)
      table.css("tr").each do |row|
        newtable = create_node(:div)
        newtable["class"] = "table"
        cells = row.css("td").to_a
        br1 = cells.first.inner_html.split(/<br>/)
        br2 = cells.last.inner_html.split(/<br>/)
        br1.each_with_index do |br, i|
          newrow = create_node(:div)
          newrow["class"] = "table-row"
          if lang1 == lang2
            cell(br, lang1, 0)
          else
            cell(br, lang1, 1)
            cell(br2[i], lang2, 2) if i < br2.size
          end
          newtable.append_child(newrow)
        end
        newdiv.append_child(newtable)
      end
      table.insert_before(newdiv)
    end
    css("table").each(&.remove!)
    self
  end

  def add_style
    style = create_node :link
    style["href"] = "../css/style.css"
    style["rel"] = "stylesheet"
    style["type"] = "text/css"
    head!.append_child style
    self
  end

  def center_style
    css("*[align=CENTER]").each do |x|
      x.attribute_remove "align"
      x.add_class "center"
    end
    self
  end

  def clean_body_tag
    body!.attributes.each { |k, v| body!.attribute_remove k }
    self
  end

  def remove_h1
    css("h1").each(&.remove!)
    self
  end

  def add_id_to_horas
    css("h2").each { |x| x["id"] = x.tag_text[3..-1].sub(/am$/, "a").sub(/as$/, "ae") }
    self
  end

  def add_htmlns
    css("html").first["xmlns"] = "http://www.w3.org/1999/xhtml"
    self
  end

  def insert_date(date)
    date_head = create_node :h1
    date_head.inner_text = date
    css("p").first.insert_before date_head
    self
  end

  def verse_numbers
    css(".text-sm.red").select(&.tag_text.matches?(/^\d/)).each(&.add_class("v-numbers"))
    self
  end

  def fix_wrong_initials
    css(".table-row").each do |row|
      ors = [] of Lexbor::Iterator::Collection
      row.css(".table-cell").each do |d|
        x = d.css(".text-xl.red")
        ors.push x unless x.empty?
      end
      next if ors.empty? # for save use .first below
      is = [] of Int32
      ors.first.each_with_index do |e, i|
        text = e.parent.as(Lexbor::Node).tag_text
        is.push(i) if text =~ /Orémus/ || 
                      text =~ /Sequéntia/ ||
                      text =~ /Allelú[ij]a./ ||
                      text =~ /Glória Patri, et Fílio, \* et Spirítui Sancto./ ||
                      text =~ /Kýrie, eléison. Christe, eléison. Kýrie, eléison./
      end
      ors.each do |o|
        is.each do |i|
          o[i]["class"] = o[i]["class"].gsub(/text-xl/, "text-lg")
        end
      end
    end
    self
  end

  def omit_comments
    if Options.opts("nocomments")
      css("span.black.text-sm").select { |x| x.tag_text =~ /^{.*}$/ }.each(&.remove!)
    end
    self
  end

  def omit_omitted
    if Options.opts("noomitted")
      css(".table-row").select { |x| x.tag_text =~ /{omittitur}/ }.each(&.remove!)
    end
    self
  end

  def collect_expands
    @@expands = @@expands + css("input[type=RADIO]").map do |x| 
      e = x["onclick"].sub(/.*\"([$&].*?)\".*/, "\\1")
      a = create_node(:a)
      a["href"] = "expands.html##{e[1..-1].gsub(/ /, "_")}"
      a.inner_html = "&nbsp;…"
      x.parent.as(Lexbor::Node).append_child(a)
      e
    end
    nodes(:input).to_a.each(&.remove!)
    self
  end

  @@expands = [] of String

  def self.get_expands
    @@expands.uniq
  end
end
