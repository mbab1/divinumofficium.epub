module Horas
  private def self.clean_string(html)
    html.gsub(/(<\/?)FONT/m, "\\1span")
        .gsub(/(<\/?FORM.*?>\n?)/m, "")
        .gsub(/(?:&nbsp;)+/, "")
        .gsub(%r{<B><I>(.)</I></B>}, "\\1")
        .gsub(/\s*~\s*/m, "<BR>")
        .gsub(/^\s*\n+/m, "")
  end

  private def self.clean_html(html, lang1, lang2)
    html = Lexbor::Parser.new(html)
                         .remove_tags(%i[style script label select a])
                         .divs(lang1, lang2)
                         .collect_expands
                         .clean_body_tag
                         .font_spans
                         .add_style
                         .center_style
                         .remove_h1
                         .add_id_to_horas
                         # .insert_date(date)
                         .fix_wrong_initials
                         .verse_numbers
                         .add_htmlns
                         .omit_omitted
                         .omit_comments
                         .to_html
    XML.parse_html(html).to_xml
       .sub("<!DOCTYPE html>","<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">")
  end

  def self.get_expands
    Lexbor::Parser.get_expands
  end

  def self.prepare_horas(html, date) : String
    html = clean_string(html)
    html = clean_html(html, Options.opts("lang1"), Options.opts("lang2"))
    begin
      File.write(Path.new(Epub::TMP_DIR, "#{date}.html"), html)
    rescue e
      abort "Can't write horas to temporary dir #{e.message}"
    end
    Lexbor::Parser.new(html).css("p").first.children.first.inner_html.sub(/<br>.*/, "")
  end

  def self.prepare_expand(html, item) : String
    html = clean_string(html)
    html = clean_html(html, Options.opts("lang1"), Options.opts("lang2"))
    phtml = Lexbor::Parser.new(html).css("body").first
    phtml.css("h3").first["id"] = item.gsub(" ", "_")
    phtml.inner_html.gsub(/<br>/, "<br/>").strip.sub(%r{<br/>$}, "")
  end
end
