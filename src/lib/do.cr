require "http/client"

module DivinumOfficium

  private def self.get(source, optstring)
    begin
    response = if md = source.match(%r{^(?:(https?)://)([^/]*)/(.*)})
                 headers = HTTP::Headers.new.add("User-Agent", "divinumofficium.epub/#{VERSION}")
                 HTTP::Client.get(source + "?" + optstring.gsub(/ /, "&"), headers).body
               else
                 `#{source} '#{(optstring)}'`.sub(/^.*?</m, "<")
               end
    rescue ex
      abort ex.message
    end
    response
  end

  private def self.horas_opts(date)
    o = [ "date=#{date}" ]
    o.push "lang1=#{Options.opts("lang1")}"
    o.push "lang2=#{Options.opts("lang2").as(String).sub(%r{.*/}, "")}"
    o.push "langfb=#{Options.opts("langfb").as(String).sub(%r{.*/}, "")}"
    %w[priest oldhymns nonumbers nofancychars].each { |a| o.push "#{a}=1" if Options.opts(a) }
    o.push "version=#{Options.opts("rubrics")}"
    o.push "command=pray#{Options.opts("horas")}"
    unless Options.opts("votive") == "Hodie"
      o.push "votive=C#{Options.opts("votive").as(String).sub(%r{.*/}, "")}"
    end
    o.push "expand=psalteria" if Options.opts("noexpand")
    o.join("&")
  end

  private def self.popup_opts(item)
    item = item.sub(/^&/, "\\&")
    o = [ "popup=#{item}" ]
    o.push "date=#{Options.opts("dateto").as(Time).to_s("%-m/%-d/%Y")}"
    o.push "lang1=#{Options.opts("lang1")}"
    o.push "lang2=#{Options.opts("lang2").as(String).sub(%r{.*/}, "")}"
    o.push "version=#{Options.opts("rubrics")}"
    o.push "priest=1" if Options.opts("priest")
    o.join("&")
  end

  def self.get_horas(date)
    get("#{Options.opts("source").as(String)}officium.pl", horas_opts(date))
  end

  def self.get_popup(item)
    get("#{Options.opts("source").as(String)}popup.pl", popup_opts(item))
  end

  def self.download_horas
    d1 = Options.opts("datefrom").as(Time)
    d2 = Options.opts("dateto").as(Time)

    if d1.date > d2.date
      abort  "Start date #{d1.to_s(Options::DO_DATEFORMAT)} greater then end date #{d2.to_s(Options::DO_DATEFORMAT)}"
    end

    Epub.create_tmp_dir
    at_exit { Epub.delete_tmp_dir }

    ordo = Hash(String, String).new

    while d1.date <= d2.date
      d = d1.to_s(Options::DO_DATEFORMAT)
      Reporter.report("Downloading #{d}")
      ordo[d] = Horas.prepare_horas(get_horas(d), d)
      d1 += 1.day
      break unless Options.opts("votive") == "Hodie"
    end

    ordo
  end

  def self.download_expands
    Reporter.report("Downloading expands")
    expands = Lexbor::Parser.get_expands
    expands.unshift("$Ante", "$Post").uniq! if Options.opts("rubrics") =~ /Divino|1910/ || Options.opts("antepost")
    expands.map { |e| Horas.prepare_expand(get_popup(e), e[1..-1]) }.join("")
  end
end
