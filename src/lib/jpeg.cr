# based on https://stackoverflow.com/questions/2450906/is-there-a-simple-way-to-get-image-dimensions-in-ruby
class IO
  def next
    c = read_byte
    loop do
      break unless c != 0xFF
      c = read_byte
    end
    loop do
      c = read_byte 
      break unless c == 0xFF
    end
    c
  end
end

module JPEG
  macro readint
    io.read_bytes(Int16, IO::ByteFormat::BigEndian)
  end

  macro readbyte
    io.read_byte.as(UInt8)
  end

  def self.get_width_and_height(io)
    raise "Not JPEG" unless io.read_byte == 0xFF && io.read_byte == 0xD8 # SOI
    width = height = 0

    while marker = io.next
      case marker
        when 0xC0..0xC3, 0xC5..0xC7, 0xC9..0xCB, 0xCD..0xCF # SOF markers
          length = readint
          _ = readbyte
          height = readint
          width = readint
          components = readbyte
          raise "Malformed JPEG" unless length == 8 + components * 3
        when 0xD9, 0xDA
          break # EOI, SOS
        else              
          slice = Bytes.new(readint - 2)
          io.read(slice)
      end
    end
    [width, height]
  end
end
