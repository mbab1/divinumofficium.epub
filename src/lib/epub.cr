require "compress/zip"
require "file_utils"
require "uuid"
require "xml"

module Epub
  TMP_DIR = Path.new Dir.tempdir, "#{Path[PROGRAM_NAME].basename}-tmp-#{Time.local.to_s("%s")}"
  EXPAND_PAGE = "expands.html"
  EPUB_IDENTIFIER = UUID.random.to_s

  def self.create_tmp_dir
    FileUtils.mkdir_p TMP_DIR
  rescue
    abort "Can't create temporary directory #{TMP_DIR}"
  end

  def self.delete_tmp_dir
    FileUtils.rm_rf TMP_DIR
  end

  private def self.container_xml
    XML.build(indent: "  ") do |xml|
      xml.element("container",
                   version: "1.0",
                   xmlns: "urn:oasis:names:tc:opendocument:xmlns:container") do
        xml.element("rootfiles") do
          xml.element("rootfile",
                      "full-path": "OEBPS/content.opf",
                      "media-type": "application/oebps-package+xml")
        end
      end
    end
  end

  private def self.content_opf(ordo, exps, fonts)
    XML.build(indent: "  ") do |xml|
      xml.element("package",
                    xmlns: "http://www.idpf.org/2007/opf",
                    "unique-identifier": "bookid",
                    version: "2.0") do
        xml.element("metadata",
                    "xmlns:dc": "http://purl.org/dc/elements/1.1/",
                    "xmlns:opf": "http://www.idpf.org/2007/opf") do
        xml.element("dc:identifier", id: "bookid") { xml.text EPUB_IDENTIFIER  }
            xml.element("dc:contributor", "opf:role": "bkp")
            xml.element("dc:date", "opf:event": "creation") { xml.text Time.local.to_s("%Y-%m-%d") }
            xml.element("dc:creator", "opf:role": "aut") { xml.text "www.divinumofficium.com" }
            xml.element("dc:language") { xml.text "la" }
            xml.element("dc:title") { xml.text Options.opts("title").as(String) }
            xml.element("meta", name: "cover", content: "cover") unless Options.opts("nocover")
          end
        xml.element("manifest") do
          xml.element("item", id: "ncx", href: "toc.ncx", "media-type": "application/x-dtbncx+xml")
          xml.element("item", id: "cover", href: "images/cover.jpg", "media-type": "image/jpeg") unless Options.opts("nocover")
          xml.element("item", id: "coverpage", href: "Text/coverpage.html", "media-type": "application/xhtml+xml") unless Options.opts("nocoverpage")
          xml.element("item", id: "titlepage", href: "Text/titlepage.html", "media-type": "application/xhtml+xml") unless Options.opts("notitlepage")
          xml.element("item", id: "indexpage", href: "Text/indexpage.html", "media-type": "application/xhtml+xml") if Options.opts("index")
          fonts.each { |font| xml.element("item", id: font, href: "Fonts/#{font}", "media-type": "font/#{File.extname(font)[1..-1]}") }
          xml.element("item", id: "style", href: "css/style.css", "media-type": "text/css")
          i = 1
          ordo.each_key do |k|
            xml.element("item", id: "B#{i}", href: "Text/#{k}.html", "media-type": "application/xhtml+xml")
            i += 1
          end
          xml.element("item", id: "expandspage", href: "Text/expands.html", "media-type": "application/xhtml+xml") unless exps.empty?
        end
        xml.element("spine", toc: "ncx") do
          xml.element("itemref", idref: "coverpage") unless Options.opts("nocoverpage")
          xml.element("itemref", idref: "titlepage") unless Options.opts("notitlepage")
          xml.element("itemref", idref: "indexpage") if Options.opts("index")
          ordo.each_with_index { |_, i| xml.element("itemref", idref: "B#{i+1}") }
          xml.element("itemref", idref: "expandspage") unless exps.empty?
        end
        xml.element("guide") do
          xml.element("reference", type: "cover", title: "Cover", href: "Text/coverpage.html")
        end unless Options.opts("nocoverpage")
      end
   end
  end

  private def self.horas
    horas = Options.opts("horas").as(String).split(/(?=\p{Lu}\p{Ll}+)/)
    if horas.first == "Omnes"
      horas = %w[Matutinum Laudes Prima Tertia Sexta Nona Vesperae Completorium]
      horas = %w[Matutinum Laudes Vesperae] if Options.opts("votive") == "Defunctorum"
    end
    horas
  end

  private macro nav_point(src, text)
    xml.element("navPoint", id: "navPoint-#{i}", playOrder: i) do
      xml.element("navLabel") do
        xml.element("text") { xml.text {{text}} }
      end
      xml.element("content", src: "Text/#{{{src}}}")
    end
    i += 1
  end

  private def self.index_entry(k, v)
    t = v
    if Options.opts("votive") == "Hodie"
      t = "#{Time.parse_local(k, Options::DO_DATEFORMAT).to_s("%b %-d")} #{t}"
    end
    t
  end

  private def self.toc_ncx(ordo, exps)
    XML.build(indent: "  ") do |xml|
      xml.element("ncx",
                xmlns: "http://www.daisy.org/z3986/2005/ncx/",
                version: "2005-1") do
        xml.element("head") do
          xml.element("meta", name: "dtb:uid", content: EPUB_IDENTIFIER)
          xml.element("meta", name: "dtb:depth", content: "2")
          xml.element("meta", name: "dtb:totalPageCount", content: "-1")
          xml.element("meta", name: "dtb:maxPageNumber", content: "-1")
        end
        xml.element("docTitle") do
          xml.element("text") { xml.text Options.opts("title").as(String) }
        end
        xml.element("navMap") do
          i = 0
          nav_point("coverpage.html", "Cover page") unless Options.opts("nocoverpage")
          i = 1
          nav_point("titlepage.html", "Title page") unless Options.opts("notitlepage")
          nav_point("indexpage.html", "Table of contents") if Options.opts("index")
          ordo.each do |k, v|
            xml.element("navPoint", id: "navPoint-#{i}", playOrder: i) do
              xml.element("navLabel") do
                xml.element("text") { xml.text index_entry(k, v) }
              end
              xml.element("content", src: "Text/#{k}.html")
              i += 1
              horas.each do |h|
                nav_point("#{k}.html##{h}", h)
              end
            end
          end
          nav_point("expands.html", "Orationes") if exps
        end
      end
    end
  end

  private def self.add_mimetype(zip)
    mimetype_text = "application/epub+zip"
    mimetype = Compress::Zip::Writer::Entry.new("mimetype")
    mimetype.compression_method = Compress::Zip::CompressionMethod::STORED
    mimetype.crc32 = Digest::CRC32.checksum(mimetype_text)
    mimetype.compressed_size = 20_u32
    mimetype.uncompressed_size = 20_u32
    zip.add(mimetype) { |io| io << mimetype_text }
  end

  private def self.index_page(ordo)
    _html_page("Table of contents") do |xml|
      xml.element("h2") { xml.text "Table of Contents" }
      xml.element("ul") do
        ordo.each do |k, v|
          xml.element("li", class: "toc-day text-sm") do
            xml.element("a", href: "#{k}.html") { xml.text index_entry(k, v) }
            xml.element("ul") do
              horas.each do |h|
                xml.element("li", class: "toc-hora") do
                  xml.element("a", href: "#{k}.html##{h}") { xml.text "[ #{h[0..2]} ]" }
                end
              end
            end
          end
        end
      end
    end
  end

  private def self.expands_page(exps)
    _html_page("Orationes") { }.sub(%r{<body/>}, "<body>#{exps}</body>")
  end

  private def self.cover_page
    begin
      width, height = JPEG.get_width_and_height(get_file("cover"))
    rescue e
      abort "Cover error: #{e.message}"
    end
    _html_page("Cover page") do |xml|
      xml.element("div", style: "text-align: center; padding: 0pt; margin: 0pt;") do
        xml.element("svg",
                    xmlns: "http://www.w3.org/2000/svg",
                    height: "100%",
                    preserveAspectRatio: "xMidYMid meet",
                    version: "1.1",
                    viewBox: "0 0 #{width} #{height}",
                    width: "100%",
                    "xmlns:xlink": "http://www.w3.org/1999/xlink") do
          xml.element("image",
                      width: width, height: height,
                      "xlink:href": "../images/cover.jpg")
        end
      end
    end
  end

  private def self.read_file(name)
    get_file(name).gets_to_end
  end

  private def self.get_file(name)
    file = Options.opts(name)
    if file
      begin
        File.open(file.as(String))
      rescue e
        STDERR.puts "Can't read #{name} file #{file}"
        raise e
      end
    else
      if name == "cover"
        Assets.get_file "cover.jpg"
      elsif name == "style"
        Assets.get_file "style.css"
      else
        abort "Internal error wrong name = #{name}"
      end
    end
  end

  private def self._html_page(title, &body)
    XML.build(indent: "  ") do |xml|
      xml.dtd("html", "-//W3C//DTD XHTML 1.1//EN",
                      "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd")
      xml.element("html", xmlns: "http://www.w3.org/1999/xhtml") do
        xml.element("head") do
          xml.element("title") { xml.text title }
          xml.element("link", href: "../css/style.css",
                              rel: "stylesheet", type: "text/css")
        end
        xml.element("body") do
          yield xml
        end
      end
    end
  end

  private def self.title_page(ordo)
    _html_page("Title page") do |xml|
      xml.element("h1") { xml.text Options.opts("title").as(String) }
      xml.element("h2") { xml.text Options.opts("rubrics").as(String).gsub(/\+/, " ") }
      xml.element("p", class: "center", style: "padding-top: 10%") { 
        xml.text "#{Options.opts("lang1")}#{Options.opts("lang1") != Options.opts("lang2") ? " / #{Options.opts("lang2").as(String).sub(%r{/.*}, "")}" : ""}" 
      }
      xml.element("p", class: "center") { xml.text "#{ordo.keys.first} – #{ordo.keys.last}" } unless Options.opts("votive") == "Defunctorum"
      xml.element("p", class: "center text-sm", style: "padding-top: 30%") { xml.text "https://www.divinumofficium.com" }
    end
  end

  def self.make
    fonts = [] of String
    Reporter.report("Building epub")
    File.open(Options.opts("output").as(String), "w") do |file|
      Compress::Zip::Writer.open(file) do |zip|
        add_mimetype(zip)
        zip.add(Path.new("META-INF", "container.xml"), container_xml)
        zip.add(Path.new("OEBPS", "css", "style.css"), read_file("style"))
        zip.add(Path.new("OEBPS", "images", "cover.jpg"), read_file("cover")) unless Options.opts("nocover")
        zip.add(Path.new("OEBPS", "Text", "coverpage.html"), cover_page()) unless Options.opts("nocoverpage")
        if fd = Options.opts("fontdir")
          fonts = Dir.open(Options.opts("fontdir").as(String)).each_child.to_a
          fd = fd.as(String)
          Dir.open(Options.opts("fontdir").as(String)).each_child do |font|
            zip.add(Path.new("OEBPS", "Fonts", font), File.open(Path.new(fd, font)))
          end
        end
        ordo = DivinumOfficium.download_horas
        exps = DivinumOfficium.download_expands
        zip.add(Path.new("OEBPS", "content.opf"), content_opf(ordo, exps, fonts))
        zip.add(Path.new("OEBPS", "toc.ncx"), toc_ncx(ordo, !exps.empty?))
        zip.add(Path.new("OEBPS", "Text", "indexpage.html"), index_page(ordo)) if Options.opts("index")
        zip.add(Path.new("OEBPS", "Text", "titlepage.html"), title_page(ordo)) unless Options.opts("notitlepage")
        ordo.each_key do |entry|
          zip.add(Path.new("OEBPS", "Text", "#{entry}.html"), File.open(Path.new(TMP_DIR, "#{entry}.html")))
        end
        zip.add(Path.new("OEBPS", "Text", "expands.html"), expands_page(exps)) unless exps.empty?
      end
    end
    Reporter.report("Epub saved as #{Options.opts("output")}")
    print "\n" unless Options.opts("quiet")
  rescue e
    STDERR.puts "Can't write output to #{Options.opts("output")}"
    raise e
  end
end
