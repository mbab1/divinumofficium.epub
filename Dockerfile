FROM crystallang/crystal:latest-alpine

RUN apk add cmake build-base && \
    git clone https://gitlab.com/mbab1/divinumofficium.epub.git && \
    cd divinumofficium.epub && shards build --production --release --no-debug --static

FROM alpine

COPY --from=0 /divinumofficium.epub/bin/doepub .

WORKDIR /tmp

ENTRYPOINT ["/doepub"]

CMD ["--help"]
